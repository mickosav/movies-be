<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //By default, Eloquent expects created_at and updated_at columns to exist on your tables.
    //If you do not wish to have these columns automatically managed by Eloquent,
    //set the $timestamps property on your model to false
    public $timestamps = false;

    //In order to use mass assignment we need to
    //define which model attributes we want to make mass assignable
    protected $fillable = [
        'name', 'director', 'image_url', 'duration', 'release_date', 'genres'
    ];

    //Making Accessor & Mutator
    protected $casts = [
      'genres' => 'array'
    ];

    public static function search($term, $skip, $take)
    {
        return self::where('name', 'LIKE', '%'.$term.'%')->skip($skip)->take($take)->get();
    }
}
