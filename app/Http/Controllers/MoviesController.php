<?php

namespace App\Http\Controllers;

use App\Http\Requests\MovieRequest;
use App\Movie;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    public function index()
    {
        $term = request()->input('term');
        $skip = request()->input('skip', 0);
        $take = request()->input('take', Movie::get()->count());

        if ($term) {
            return Movie::search($term, $skip, $take);
        } else {
            return Movie::skip($skip)->take($take)->get();
        }
    }

    public function store(MovieRequest $request)
    {
        return Movie::create($request->all());
    }

    public function show($id)
    {
        return Movie::findOrFail($id);
    }

    public function update(MovieRequest $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->update($request->all());
        return $movie;
    }

    public function destroy($id)
    {
        Movie::destroy($id);
    }
}
